using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using LibraryTracker.Models;

namespace LibraryTracker.Controllers
{
  public class BorrowController : Controller
  {
    private static Borrow GetById(LibraryContext context, int id)
    {
      return context.Borrow.Where(x => x.ID == id).Single();
    }

    public static void ExtendDueDateForBorrowByID(int id)
    {
      using(var context = new LibraryContext())
      {
        Borrow borrow = GetById(context, id);
        borrow.DueDate = borrow.DueDate.AddDays(7);
        context.SaveChanges();
      }

    }

    public static Borrow BorrowByBookID(int bookId)
    {
      using (var context = new LibraryContext())
      {
        return context.Borrow.Where(x => x.BookID == bookId).FirstOrDefault();
      }
    }

    public static void ReturnBorrowByID(int id)
    {
      using(var context = new LibraryContext())
      {
        Borrow borrow = GetById(context, id);
        borrow.ReturnedDate = DateTime.Today;
        context.SaveChanges();
      }
    }

    public static void CreateBorrow(int bookID)
    {
      using(var context = new LibraryContext())
      {
        context.Borrow.Add(
          new Borrow { BookID=bookID, CheckedOutDate=DateTime.Today, DueDate=DateTime.Today.AddDays(14) }
        );
        context.SaveChanges();
      }
    }
  }
}
