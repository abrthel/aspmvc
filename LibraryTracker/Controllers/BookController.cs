﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics;
using LibraryTracker.Models;
using Microsoft.EntityFrameworkCore;

namespace LibraryTracker.Controllers
{
  public class BookController : Controller
  {
    public ActionResult Index()
    {
      return RedirectToAction("List");
    }

    public ActionResult List()
    {
      ViewBag.Books = GetBooks();
      return View();
    }

    public ActionResult Details(int id, string button)
    {
      if(id == 0) {
        return RedirectToAction("List");
      }

      if(Request.Method == "POST") {
        Borrow borrow;

        switch (button)
        {
          case "extendDueDate":
            borrow = BorrowController.BorrowByBookID(id);
            ExtendDueDateForBookByID(borrow.ID);
            break;
          case "returnBook":
            borrow = BorrowController.BorrowByBookID(id);
            ReturnBookByID(borrow.ID);
            break;
          case "deleteBook":
            DeleteBookById(id);
            return RedirectToAction("List");

          case "createBorrow":
            BorrowController.CreateBorrow(id);
            break;
        }
      }

      var book = GetBookByID(id);
      ViewBag.Book = book;
      ViewBag.Borrow = book.Borrows.FirstOrDefault();
      return View();
    }

    public ActionResult Create(string title, int authorId, DateTime publicationDate)
    {
      if (Request.Method == "POST")
      {
        try
        {
            var book = CreateBook(title, authorId, publicationDate);
            ViewBag.Success = $"You have successfully created {book.Title}";
        }
        catch(Exception ex)
        {
          ViewBag.Error = $"Unable create book: {ex.Message}";
        }
      }

      ViewBag.Authors = AuthorController.GetAuthors();

      return View();
    }


    public void ExtendDueDateForBookByID(int id)
    {
      BorrowController.ExtendDueDateForBorrowByID(id);
    }

    public void ReturnBookByID(int id)
    {
      BorrowController.ReturnBorrowByID(id);
    }

    public void DeleteBookById(int id)
    {
      using(var context = new LibraryContext()) {
        Book book = context.Books.Where(x => x.ID == id).SingleOrDefault();
        if(book != null)
        {
          context.Books.Remove(book);
          context.SaveChanges();
        }
      }
    }

    public ICollection<Book> GetBooks()
    {
      using(var context = new LibraryContext())
      {
        return context.Books
                        .Include(x => x.Author)
                        .Include(x => x.Borrows)
                        .ToList();
      }
    }

    public ICollection<Book> GetOverdueBooks()
    {
      using(var context = new LibraryContext())
      {
        return context.Books
                        .Include(x => x.Author)
                        .Include(x => x.Borrows)
                        .Where(x => x.Borrows.Any(y => y.DueDate < DateTime.Today))
                        .ToList();
      }

    }

    public Book GetBookByID(int id)
    {
      using(var context = new LibraryContext())
      {
        return context.Books
                        .Include(x => x.Author)
                        .Include(x => x.Borrows)
                        .Where(x => x.ID == id)
                        .SingleOrDefault();
      }
    }

    public Book CreateBook(string title, int authorId, DateTime publicationDate)
    {
        using(var context = new LibraryContext())
        {
          Book book = new Book(title, authorId, publicationDate);
          context.Books.Add(book);
          context.SaveChanges();

          return book;
        }
    }
  }
}
