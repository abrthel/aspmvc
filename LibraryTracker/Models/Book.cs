﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTracker.Models
{
  [Table("book")]
  public class Book
  {
    [Key]
    [Column(TypeName = "int(10)")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int ID { get; set; }

    [Column(TypeName = "varchar(30)")]
    [Required]
    public string Title { get; private set; }

    [Column(TypeName = "date")]
    public DateTime PublicationDate { get; private set; }

    [Column(TypeName = "int(10)")]
    public int AuthorID { get; private set; }

    [ForeignKey(nameof(AuthorID))]
    [InverseProperty(nameof(Models.Author.Books))]
    public virtual Author Author { get; set; }

    [InverseProperty(nameof(Borrow.Book))]
    public virtual ICollection<Borrow> Borrows { get; set; } = new HashSet<Borrow>();

    public Book(string title, int authorID, DateTime publicationDate) {
      Title = title;
      AuthorID = authorID;
      PublicationDate = publicationDate;
    }
  }
}
