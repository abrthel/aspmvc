using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace LibraryTracker.Models
{
  [Table("author")]
  public class Author
  {
    [Key]
    [Column(TypeName = "int(10)")]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int ID { get; set; }

    [Column(TypeName = "varchar(30)")]
    [Required]
    public string Name { get; private set; }

    [Column(TypeName = "date")]
    public DateTime BirthDate { get; private set; }

    [Column(TypeName = "date")]
    public DateTime? DeathDate { get; set; }

    [InverseProperty(nameof(Book.Author))]
    public virtual ICollection<Book> Books { get; set; }

    public Author(int ID, string name, DateTime birthDate, DateTime? deathDate = null) {
      this.ID = ID;
      Name = name;
      BirthDate = birthDate;
      DeathDate = deathDate;
      Books = new HashSet<Book>();
    }
  }
}
