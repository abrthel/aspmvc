using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace LibraryTracker.Models
{
  public class LibraryContext : DbContext
  {
    public virtual DbSet<Author> Authors { get; set; }
    public virtual DbSet<Book> Books { get; set; }
    public virtual DbSet<Borrow> Borrow { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder builder)
    {
      if(!builder.IsConfigured)
      {
        string version = "10.4.14-MariaDB";

        string connection =
          "server=localhost;" +
          "port=3306;" +
          "user=root;" +
          "password=ROOTPASS;" +
          "database=mvc_library;";

        builder.UseMySql(connection, x => x.ServerVersion(version));
      }
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Author>(entity => {
        entity
          .HasMany(b => b.Books)
          .WithOne(a => a.Author)
          .OnDelete(DeleteBehavior.Restrict);

        entity.HasData(
          new { ID = -1, Name = "Jim Butcher", BirthDate = new DateTime(1971, 10, 26) },
          new { ID = -2, Name = "George RR Martin", BirthDate = new DateTime(1948, 06, 20) },
          new { ID = -3, Name = "J. R. R. Tolkien", BirthDate = new DateTime(1892, 1, 03), DeathDate = new DateTime(1973, 02, 02) },
          new { ID = -4, Name = "Margaret Atwood", BirthDate = new DateTime(1939, 11, 18) },
          new { ID = -5, Name = "J. K. Rowling", BirthDate = new DateTime(1965, 06, 30) }
        );
      });

      modelBuilder.Entity<Book>(entity => {
        entity
          .HasMany(b => b.Borrows)
          .WithOne(a => a.Book)
          .OnDelete(DeleteBehavior.Cascade);

        entity.HasData(
          new { ID = -1, Title = "Storm Front", AuthorID=-1, PublicationDate = new DateTime(2000, 04, 01), CheckedOutDate= new DateTime(2019, 12, 25), DueDate= new DateTime(2019, 12, 25).AddDays(7), ReturnedDate=new DateTime(2019, 12, 25).AddDays(7)},
          new { ID = -2, Title = "Fool Moon", AuthorID=-1, PublicationDate = new DateTime(2001, 01, 01), CheckedOutDate= new DateTime(2019, 12, 25), DueDate= new DateTime(2019, 12, 25).AddDays(14), ReturnedDate=new DateTime(2019, 12, 25).AddDays(14)},
          new { ID = -3, Title = "Grave Peril", AuthorID=-1, PublicationDate = new DateTime(2001, 09, 01), CheckedOutDate= new DateTime(2019, 12, 25), DueDate= new DateTime(2019, 12, 25).AddDays(7)}
        );
      });


      modelBuilder.Entity<Borrow>(entity => {
        entity.HasData(
          new { ID = -1, BookID = -1, CheckedOutDate = new DateTime(2019, 12, 25), DueDate = new DateTime(2019, 12, 25).AddDays(7), ReturnedDate = new DateTime(2019, 12, 25).AddDays(7) },
          new { ID = -2, BookID = -2, CheckedOutDate = new DateTime(2019, 12, 25), DueDate = new DateTime(2019, 12, 25).AddDays(14), ReturnedDate = new DateTime(2019, 12, 25).AddDays(14) },
          new { ID = -3, BookID = -3, CheckedOutDate = new DateTime(2019, 12, 25), DueDate = new DateTime(2019, 12, 25).AddDays(7) }
        );
      });
    }
  }
}
